package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

func main() {
	for _, v := range os.Args[1:len(os.Args)] {
		file, err := os.Open(v)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
		fmt.Println("Processing......")
		read := make(chan [][]string, 1)
		write := make(chan [][]string, 1)
		ReadFileChan(read, ReadFile(file))
		WriteFileChan(read, write)
		writeChanges(write, v)
		close(read)
		close(write)
	}

}
func ReadFileChan(c chan<- [][]string, result <-chan [][]string) {
	c <- <-result
}
func WriteFileChan(read <-chan [][]string, write chan<- [][]string) {
	writeFile := <-read
	write <- writeFile
}

func ReadFile(file *os.File) <-chan [][]string {
	c := make(chan [][]string)
	// cherr := make(chan error)

	go func() {
		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		var txtlines []string

		re, err := regexp.Compile(`^.*?:`)
		if err != nil {
			log.Fatal(err)
		}

		for scanner.Scan() {
			res := strings.ReplaceAll(scanner.Text(), ",", "|")
			trim := re.ReplaceAllString(res, "")
			txtlines = append(txtlines, trim)
		}

		file.Close()

		var final [][]string

		j := 3
		for i := 0; i < len(txtlines); i += j {
			final = append(final, txtlines[i:i+j])
		}
		c <- final
		//err1 := errors.New("math: square root of negative number")
		//cherr <- err1
	}()
	return c
}

func writeChanges(rows <-chan [][]string, filename string) {
	f, err := os.Create(fmt.Sprintf("output-%s.csv", filename))
	if err != nil {
		log.Fatal(err)
	}
	err = csv.NewWriter(f).WriteAll(<-rows)
	f.Close()
	if err != nil {
		log.Fatal(err)
	}
}
